/*
 * Custom LCD Characters
 */

/*
 * Omega Symbol
 */
 byte ohm[8] = {
  B00000,
  B01110,
  B10001,
  B10001,
  B10001,
  B01010,
  B11011
};
