/*
* Global variables
*/

//Serial data buffer
byte inc_buff[20]; //Incoming data buffer for Serial control

//Mode Arrays
byte chModeProv[4]{0,0,0,0}; //Channel Modes Provisional
byte chMode[4]{0,0,0,0}; //Channel Modes

//Relay States
bool relay[25]; //Flages for Relay State

//Relay Bitmasks
byte relayset[25]{B10000000,B00100000, B00001000, B00000010, B00000001, B00000100, B00010000, B01000000}; //Set relay bitmask
byte relayreset[25]{B01000000,B00010000, B00000100, B00000001, B00000010, B00001000, B00100000, B10000000}; //Clear relay bitmask

//Encoder Variables
long knob_prevpos  = 0; //Encoder Position Value
long knob_uppos  = 0; //Encoder updated Position Value

//Menu Item Index
byte menu_index = 0; // Menu item index

//status Flags
bool refresh_lcd = true;
bool update_setting = false; //Settings to be updated flag

 
