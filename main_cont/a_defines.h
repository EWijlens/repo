/*
 * Define Relays
 */
 
//Channel 1 2 (A/B) Relays
#define ca  relay[0] //Channel A (1) Connect Relay
#define ca2  relay[1] //Channel A (1) 2 Ohm Link Relay
#define cb  relay[2] //Channel B (2) Connect Relay
#define cb2  relay[3] //Channel B (2) 2 Ohm Link Relay

//Channel 3 4 (C/D) Relays
#define cc  relay[4] //Channel C (3) Connect Relay
#define cc2  relay[5] //Channel C (3) 2 Ohm Link Relay
#define cd  relay[6] //Channel D (4) Connect Relay
#define cd2  relay[7] //Channel D (4) 2 Ohm Link Relay

//Define Aux/Optional Relays, Only applicable when more than one relay board is connected
#define rly14 relay[13]
#define rly15 relay[14]
#define rly16 relay[15]
#define rly17 relay[16]
#define rly18 relay[17]
#define rly19 relay[18]
#define rly20 relay[19]
#define rly21 relay[20]
#define rly22 relay[21]
#define rly23 relay[22]
#define rly24 relay[23]
#define rly25 relay[24]

/*
 * Packet Byte Structure
 */

#define cmd inc_buff[0]  //Command Byte
#define id1 inc_buff[1]  //ID 1 Byte
#define id2 inc_buff[2]  //ID 2 Byte
#define val inc_buff[3]  //Value Byte

/*
 * I2C LEDMUX Registers
 */
#define ADDRLDMUX1 0x68 //Address

#define MODE1 0x00
#define MODE2 0x01
#define PWM0  0x02
#define PWM1  0x03
#define PWM2  0x04
#define PWM3  0x05
#define PWM4  0x06
#define PWM5  0x07
#define PWM6  0x08
#define PWM7  0x09
#define PWM8  0x0A
#define PWM9  0x0B
#define PWM10 0x0C
#define PWM11 0x0D
#define PWM12 0x0E
#define PWM13 0x0F
#define PWM14 0x10
#define PWM15 0x11
#define GRPPWM  0x12
#define GRPFREQ 0x13
#define LEDOUT0 0x14
#define LEDOUT1 0x15
#define LEDOUT2 0x16
#define LEDOUT3 0x17
#define SUBADR1 0x18
#define SUBADR2 0x19
#define SUBADR3 0x1A
#define ALLCALLADR  0x1B
#define IREF  0x1C
#define EFLAG1  0x1D
#define EFLAG2  0x1E

/*
 * I2C GPIO Expander Registers
 */

 #define ADDR_GPIO1 0x20 //7 bit GPIOex 1 Address
 #define ADDR_GPIO2 0x21 //7 bit GPIOex 2 Address

 //Define Control Registers
 #define IOCON 0x05

//Define Port Registers
 #define IODIRA 0x00
 #define IODIRB 0x01
 #define IPOLA 0x02
 #define IPOLB 0x03
 #define GPINTENA 0x04
 #define GPINTENB 0x05
 #define GPPUA 0x0C
 #define GPPUB 0x0D
 #define GPIOA 0x12
 #define GPIOB 0x13
 #define OLATA 0x14
 #define OLATB 0x15

 
 
 
 

 




































 
 
 
