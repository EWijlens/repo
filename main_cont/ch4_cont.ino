/*
Channel 4 Controls
*/

void ch4_mode()
{
    if ( chMode[3] == 2 )  //2 Ohm Relay config handler
    {
        Serial.println("Ch4 2 Ohm");
        cd2 = true; //Set 2 ohm relay link
        cd = true; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else if ( chMode[3] == 4 )  //4 Ohm Relay config handler
    {
        Serial.println("Ch4 4 Ohm");
        cd2 = false; //Set 2 ohm relay link
        cd = true; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else if ( chMode[3] == 0 )  //Disable Relay config handler
    {
        Serial.println("Ch4 Off");
        cd2 = false; //Set 2 ohm relay link
        cd = false; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else
    {
        Serial.println("Invalid Mode Control Command");  //Debug error msg
    }

}
