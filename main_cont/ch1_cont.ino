/*
Channel 1 Controls
*/

void ch1_mode()
{
    if ( chMode[0] == 2 )  //2 Ohm Relay config handler
    {
        Serial.println("Ch1 2 Ohm");
        ca2 = true; //Set 2 ohm relay link
        ca = true; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else if ( chMode[0] == 4 )  //4 Ohm Relay config handler
    {
        Serial.println("Ch1 4 Ohm");
        ca2 = false; //Set 2 ohm relay link
        ca = true; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else if ( chMode[0] == 0 )  //Disable Relay config handler
    {
        Serial.println("Ch1 Off");
        ca2 = false; //Set 2 ohm relay link
        ca = false; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else
    {
        Serial.println("Invalid Mode Control Command");  //Debug error msg
    }

}
