/*
* Mode Settings
*/

void mode_set(int ch_num, byte ch_md)
{
    Serial.println((String)"Channel Selected is: "+ch_num+" Value: "+ch_md);  //Debug msg showing selected channel and mode
    
    if(ch_num==1)  //Channel 1 Mode Handler
    {
        chMode[0]= ch_md;
        ch1_mode();
    }

    if(ch_num==2)  //Channel 2 Mode Handler
    {
        chMode[1]= ch_md;
        ch2_mode();
    }

    if(ch_num==3)  //Channel 3 Mode Handler
    {
        chMode[2]= ch_md;
        ch3_mode();
    }

    if(ch_num==4)  //Channel 4 Mode Handler
    {
        chMode[3]= ch_md;
        ch4_mode();
    }
}
