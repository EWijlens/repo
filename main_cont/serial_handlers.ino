
/*
*Serial Handlers for control interface
*/


/*
* Incoming serial handler
*/
void serialEvent()
{

    while( Serial.available() )
    {
        Serial.readBytesUntil('!',inc_buff,10);
        command_handler();
    }
}

/*
* Buffer handler
*/
void command_handler()
{
    int ch_num;
    byte ch_md;

    if( cmd=='c'||cmd=='C' )  //Control command
    {
        Serial.println("Write Control Command Recieved"); //Debug msg
    }

    if( cmd=='r'||cmd=='R' )  //Read command
    {
        Serial.println("Read Command Recieved"); //Debug msg
    }

    if( cmd=='m'||cmd=='M' )  //Mode command
    {
        Serial.println("Write Mode Command Recieved"); //Debug msg
    

        if( id1=='c' || id1=='C' )
        {
            ch_num = id2-48; //Convert from ASCII to DEC
            ch_md = val-48;  //Convert from ASCII to DEC
        
            if( ch_num>0 && ch_num<5 ) //Test to ensure channel selection is between 1 & 4
            {
                mode_set(ch_num, ch_md);
            }
            else
            {
                Serial.println("Invalid Channel");  //Debug msg
            }
        }

        else
        {
            Serial.println("Invalid Mode Control Command");
        }
    }


    if( cmd=='h'||cmd=='H' )  //Help Command
    {
        help();
    }
}
