/*
* TLC59116 Driver
*/

void init_TLC()
{
    byte md1=0;
    int y =0;

    Wire2.beginTransmission(ADDRLDMUX1);
    Wire2.write(MODE1);
    Wire2.endTransmission();

    Wire2.requestFrom(ADDRLDMUX1,1);
    md1=Wire2.read();
    Serial.println(md1,BIN);
    md1 &= B11101111;

    Wire2.beginTransmission(ADDRLDMUX1);
    Wire2.write(MODE1);
    Wire2.write(md1);
    Wire2.endTransmission();

    Wire2.beginTransmission(ADDRLDMUX1);
    // Wire2.write(PWM0);
    Wire2.write(MODE1);
    Wire2.endTransmission();

    Wire2.requestFrom(ADDRLDMUX1,1);
    md1=Wire2.read();
    Serial.println(md1,BIN);

    //Flash LED to test OP 
    while(y<10)
    {
        Wire2.beginTransmission(ADDRLDMUX1);
        Wire2.write(LEDOUT0);
        Wire2.write(0x01);
        Wire2.endTransmission();
        delay(200);
        Wire2.beginTransmission(ADDRLDMUX1);
        Wire2.write(LEDOUT0);
        Wire2.write(0x00);
        Wire2.endTransmission();
        delay(200);
        y++;
    }
}
