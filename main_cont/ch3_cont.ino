/*
Channel 3 Controls
*/

void ch3_mode()
{
    if ( chMode[2] == 2 )  //2 Ohm Relay config handler
    {
        Serial.println("Ch3 2 Ohm");
        cc2 = true; //Set 2 ohm relay link
        cc = true; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else if ( chMode[2] == 4 )  //4 Ohm Relay config handler
    {
        Serial.println("Ch3 4 Ohm");
        cc2 = false; //Set 2 ohm relay link
        cc = true; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else if ( chMode[2] == 0 )  //Disable Relay config handler
    {
        Serial.println("Ch3 Off");
        cc2 = false; //Set 2 ohm relay link
        cc = false; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else
    {
        Serial.println("Invalid Mode Control Command");  //Debug error msg
    }

}
