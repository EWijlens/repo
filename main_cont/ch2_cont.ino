/*
Channel 2 Controls
*/

void ch2_mode()
{
    if ( chMode[1] == 2 )  //2 Ohm Relay config handler
    {
        Serial.println("Ch2 2 Ohm");
        cb2 = true; //Set 2 ohm relay link
        cb = true; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else if ( chMode[1] == 4 )  //4 Ohm Relay config handler
    {
        Serial.println("Ch2 4 Ohm");
        cb2 = false; //Set 2 ohm relay link
        cb = true; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else if ( chMode[1] == 0 )  //Disable Relay config handler
    {
        Serial.println("Ch2 Off");
        cb2 = false; //Set 2 ohm relay link
        cb = false; //Connect to OP
        MCP_update(ADDR_GPIO1);
        refresh_lcd = true;
    }

    else
    {
        Serial.println("Invalid Mode Control Command");  //Debug error msg
    }

}
