#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>
#include <LiquidCrystal.h>
#include <i2c_t3.h>
#include "a_defines.h"
#include "a_globals.h"
#include "custom_chars.h"

//Instantiate LCD
LiquidCrystal lcd(22, 23, 21, 20, 19, 18);

//Instantiate Encoder
Encoder user_knob(15,14);
/*
   Setup
*/
void setup()
{
  // put your setup code here, to run once:

  //Config Pins
  //pinMode(13, OUTPUT); //Configure pin 13 as OP
  pinMode(13,INPUT); //Configure pin 29 as IP for encoder click
  attachInterrupt(digitalPinToInterrupt(13), ISR1, RISING);

  //Config Comm busses
  Serial.begin(115200); //Initiate SP for debug and control
  Wire2.begin(); //Start I2C Bus

  //Config LCD
  lcd.begin(16,2); //Start LCD 
  lcd.createChar(0, ohm); //Create custom char for omega symbol 
  lcd.clear(); //Clear screen
  lcd.setCursor(0,0);
  lcd.display();
  lcd.print("....FUS LOAD....");
  lcd.setCursor(4,1);
  lcd.print("Ver 0.1");


  //Config I2C devices
  //init_TLC(); //Initilaise I2C LED expander/controller
  init_MCP(); //Initialise I2C GPIO Expander's on relay control board


  //Print First Console 
  delay(5000);
  Serial.println("...Fusion Load Box...");
  Serial.println("Initialize Peripherals Successful");
  Serial.write("\n"); //New Line
  Serial.println("'c' or 'C' Control, NOT IMPLEMENTED");
  Serial.println("'r' or 'R' Read, NOT IMPLEMENTED");
  Serial.println("'m' or 'M' Mode");
  Serial.println("'h' or 'H' Help, NOT IMPLEMENTED");
  Serial.println("---------------------------------");
  //delay(5000);

}

/*
   Main
*/

void loop()
{
// put your main code here, to run repeatedly:
  knob_uppos = user_knob.read();
  knob_uppos = knob_uppos/2;

  if(menu_index == 0)
  {
  //Print First UI Display
  if(refresh_lcd)
  {
    lcd.clear(); //Clear screen
    refresh_lcd = false;
  }

  if(update_setting)
  {
    int x;
    for(x=0; x<4; x++)
    {
      chMode[x] = chModeProv[x];
    }
    ch1_mode();
    ch2_mode();
    ch3_mode();
    ch4_mode();
    update_setting = false;
  }
    int x;
    for(x=0; x<4; x++)
    {
      chModeProv[x] = chMode[x];
    }
  
  lcd.setCursor(0,0);
  lcd.print("Ch1: ");
  lcd.setCursor(4,0);
  if(chMode[0]>0){lcd.print(chMode[0]);}
  if(chMode[0]==0){lcd.print("x");}
  lcd.setCursor(5,0);
  lcd.write(byte(0));
  
  lcd.setCursor(9,0);
  lcd.print("Ch2: ");
  lcd.setCursor(13,0);
  if(chMode[1]>0){lcd.print(chMode[1]);}
  if(chMode[1]==0){lcd.print("x");}
  lcd.setCursor(14,0);
  lcd.write(byte(0));
  
  lcd.setCursor(0,1);
  lcd.print("Ch3: ");
  lcd.setCursor(4,1);  
  if(chMode[2]>0){lcd.print(chMode[2]);}
  if(chMode[2]==0){lcd.print("x");}
  lcd.setCursor(5,1);
  lcd.write(byte(0));
  
  lcd.setCursor(9,1);
  lcd.print("Ch4: ");
  lcd.setCursor(13,1);  
  if(chMode[3]>0){lcd.print(chMode[3]);}
  if(chMode[3]==0){lcd.print("x");}
  lcd.setCursor(14,1);
  lcd.write(byte(0));
  
  }
  if(menu_index >0){menu();}

  
  if (knob_uppos > knob_prevpos) 
  {
    Serial.print("Right");
    //Serial.print(knob_prevpos);
    Serial.write("\n"); //New Line
    knob_prevpos = knob_uppos;
  }
  if (knob_uppos < knob_prevpos) 
  {
    Serial.print("Left");
    Serial.write("\n"); //New Line
    knob_prevpos = knob_uppos;
  }

}

void ISR1()
{
  detachInterrupt(digitalPinToInterrupt(13));
  delay(500);
  Serial.print("Click");
  menu_index = menu_index+1;
  if(menu_index>5){menu_index=0;}
  refresh_lcd = true;
  attachInterrupt(digitalPinToInterrupt(13), ISR1, LOW);
}

void menu()
{ 
  while(menu_index>0)
  {
  knob_uppos = user_knob.read();
  knob_uppos = knob_uppos/2;
  
    if(refresh_lcd)
    {
      lcd.clear(); //Clear screen
      refresh_lcd = false;
    }
    
    if(menu_index == 1)
    {
      if(knob_uppos>knob_prevpos)
      {
        chModeProv[0]=chModeProv[0]+2;
        if(chModeProv[0]>4){chModeProv[0]=4;}
        knob_prevpos = knob_uppos;
      }
      if(knob_uppos<knob_prevpos && chModeProv[0]!=0)
      {
        chModeProv[0]=chModeProv[0]-2;
        if(chModeProv[0]==0){chModeProv[0]=0;}
        knob_prevpos = knob_uppos;
      }
      knob_prevpos = knob_uppos;
      
      lcd.setCursor(0,0);
      lcd.print("<--Setup  Ch1-->");
      lcd.setCursor(0,1);
      lcd.print("    Mode =");
      lcd.setCursor(11,1);
      if(chModeProv[0]>0){lcd.print(chModeProv[0]);}
      if(chModeProv[0]==0){lcd.print("x");}
      lcd.setCursor(12,1);
      lcd.write(byte(0));
    }
    if(menu_index == 2)
    {
      if(knob_uppos>knob_prevpos)
      {
        chModeProv[1]=chModeProv[1]+2;
        if(chModeProv[1]>4){chModeProv[1]=4;}
        knob_prevpos = knob_uppos;
      }
      if(knob_uppos<knob_prevpos && chModeProv[1]!=0)
      {
        chModeProv[1]=chModeProv[1]-2;
        if(chModeProv[1]==0){chModeProv[1]=0;}
        knob_prevpos = knob_uppos;
      }
      knob_prevpos = knob_uppos;
      
      lcd.setCursor(0,0);
      lcd.print("<--Setup  Ch2-->");
      lcd.setCursor(0,1);
      lcd.print("    Mode =");
      lcd.setCursor(11,1);
      if(chModeProv[1]>0){lcd.print(chModeProv[1]);}
      if(chModeProv[1]==0){lcd.print("x");}
      lcd.setCursor(12,1);
      lcd.write(byte(0));
    }
    if(menu_index == 3)
    {
      if(knob_uppos>knob_prevpos)
      {
        chModeProv[2]=chModeProv[2]+2;
        if(chModeProv[2]>4){chModeProv[2]=4;}
        knob_prevpos = knob_uppos;
      }
      if(knob_uppos<knob_prevpos && chModeProv[2]!=0)
      {
        chModeProv[2]=chModeProv[2]-2;
        if(chModeProv[2]==0){chModeProv[2]=0;}
        knob_prevpos = knob_uppos;
      }
      knob_prevpos = knob_uppos;
      
      lcd.setCursor(0,0);
      lcd.print("<--Setup  Ch3-->");
      lcd.setCursor(0,1);
      lcd.print("    Mode =");
      lcd.setCursor(11,1);
      if(chModeProv[2]>0){lcd.print(chModeProv[2]);}
      if(chModeProv[2]==0){lcd.print("x");}
      lcd.setCursor(12,1);
      lcd.write(byte(0));
    }
    if(menu_index == 4)
    {      
      if(knob_uppos>knob_prevpos)
      {
        chModeProv[3]=chModeProv[3]+2;
        if(chModeProv[3]>4){chModeProv[3]=4;}
        knob_prevpos = knob_uppos;
      }
      if(knob_uppos<knob_prevpos && chModeProv[3]!=0)
      {
        chModeProv[3]=chModeProv[3]-2;
        if(chModeProv[3]==0){chModeProv[3]=0;}
        knob_prevpos = knob_uppos;
      }
      knob_prevpos = knob_uppos;
      
      lcd.setCursor(0,0);
      lcd.print("<--Setup  Ch4-->");
      lcd.setCursor(0,1);
      lcd.print("    Mode =");
      lcd.setCursor(11,1);
      if(chModeProv[3]>0){lcd.print(chModeProv[3]);}
      if(chModeProv[3]==0){lcd.print("x");}
      lcd.setCursor(12,1);
      lcd.write(byte(0));
    }
    if(menu_index == 5)
    {
    lcd.setCursor(0,0);
    lcd.print("<----Accept---->");
    if(knob_uppos>knob_prevpos) 
      {
        lcd.setCursor(6,1);
        lcd.print("Yes");
        knob_prevpos = knob_uppos;
        update_setting = true;
      }
      if(knob_uppos<knob_prevpos)
      {
        lcd.setCursor(6,1);
        lcd.print(" No");
        knob_prevpos = knob_uppos;
        update_setting = false;
      }

    }
  }
}

/*
   Help Menu
*/
void help()
{

}

/*
   Relay Write Bits
*/
void rly_write()
{


}
