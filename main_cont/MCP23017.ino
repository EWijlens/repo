
/*
 * Init MCP23017
 */
void init_MCP()
{
    Serial.println("Initialize MCP23017");  //Debug MSG

    //Set up IO direction register
    Wire2.beginTransmission(ADDR_GPIO1);
    Wire2.write(IODIRA); //Port A IODIR Register
    Wire2.write(B00000000); //Value to write to IODIRA register
    Wire2.write(B00000000); //Value to write to IODIRB register
    Wire2.endTransmission();
    Serial.println("IODIRA Port A direction written");

    //Clear MCP OP
    Wire2.beginTransmission(ADDR_GPIO1);
    Wire2.write(OLATA); //Port A Latch Register
    Wire2.write(B01010101); //Value to write to OLATA register
    Wire2.write(B10101010); //Value to write to OLATB register
    Wire2.endTransmission();
    delay(500);
    Wire2.beginTransmission(ADDR_GPIO1);
    Wire2.write(OLATA); //Port A Latch Register
    Wire2.write(B00000000); //Value to write to OLATA register
    Wire2.write(B00000000); //Value to write to OLATB register
    Wire2.endTransmission();

    Serial.println("OLATA Port A OLAT Written");  //Debug MSG
}

/*
 * Update MCP23017 output
 */
void MCP_update(byte address)
{
    Serial.println("MCP Update");  //Debug MSG
    int x = 0;
    byte PORTA = 0;
    Serial.println("\nPORT A");  //Debug MSG
    Serial.print(PORTA, BIN);  //Debug MSG
    Serial.println("\n");  //Debug MSG

    while ( x <= 3 )
    {
        if ( relay[x] == false )
        {
            PORTA |= relayreset[x];
        }
        x++;
    }
    x = 0;
    while ( x <= 3 )
    {
        if ( relay[x] == true )
        {
        PORTA |= relayset[x];
        }
        x++;
    }
    Serial.println("\nPORT A");  //Debug MSG
    Serial.print(PORTA, BIN);  //Debug MSG
    Serial.println("\n");  //Debug MSG

    x = 4;
    byte PORTB = 0;
    //byte PORTB = 0;
    //MCP_reset(address);
    while ( x <= 7 )
    {
        if ( relay[x] == false )
        {
            PORTB |= relayreset[x];
        }
        x++;
    }
    x = 4;
    while ( x <= 7 )
    {
        if ( relay[x] == true )
        {
            PORTB |= relayset[x];
        }
        x++;
    }
    Serial.println("PORT B");  //Debug MSG
    Serial.print(PORTB, BIN);  //Debug MSG
    Serial.println("\n");  //Debug MSG

    Wire2.beginTransmission(ADDR_GPIO1);
    Wire2.write(OLATA); //Port A Latch Register
    Wire2.write(PORTA); //Value to write to OLATA register
    Wire2.endTransmission();
    delay(100);
    Wire2.beginTransmission(ADDR_GPIO1);
    Wire2.write(OLATB); //Port B Latch Register
    Wire2.write(PORTB); //Value to write to OLATB register
    Wire2.endTransmission();

    delay(100);

    Wire2.beginTransmission(ADDR_GPIO1);
    Wire2.write(OLATA); //Port A Latch Register
    Wire2.write(B00000000); //Value to write to OLATA register
    Wire2.endTransmission();
    delay(100);
    Wire2.beginTransmission(ADDR_GPIO1);
    Wire2.write(OLATB); //Port A Latch Register
    Wire2.write(B00000000); //Value to write to OLATA register
    Wire2.endTransmission();

    Serial.println("OLATA Port A OLAT Written");  //Debug MSG
    Serial.println("OLATB Port B OLAT Written");  //Debug MSG
}
